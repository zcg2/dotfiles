# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

[ -z "$TMUX"  ] && export TERM="xterm-256color"
[[ -z "$TMUX" ]] && exec tmux -2 -f ~/.config/tmux/conf

include () {
	[[ -f "$1" ]] && source "$1"
}

include_path_dir () {
        [[ -d "$1"  ]] && export PATH="$1:$PATH"
}

include "${HOME}/.config/bash/aliases"
include "${HOME}/.config/bash/env"
include "${HOME}/.local/bash/env"
include "${HOME}/.config/bash/functions"
include "${HOME}/.config/bash/prompt"

include_path_dir "$HOME/.config/bash/.local/bin"
include_path_dir "$HOME/.local/bin"
include_path_dir "$HOME/.cargo/bin"
include_path_dir "/snap/bin"

# [ -f ~/.fzf.bash ] && source ~/.fzf.bash
