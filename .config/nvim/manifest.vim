call plug#begin('~/.local/share/nvim/plugged')
" Plug 'Shougo/denite.nvim', { 'do': ':UpdateRemotePlugins' }  " dark-powered unite.vim
Plug 'nixprime/cpsm'  " A CtrlP matcher, specialized for paths. 
Plug 'w0rp/ale' " asynchronous linting
Plug 'airblade/vim-gitgutter' " Show linewise git status
Plug 'tpope/vim-fugitive' " Convenient git mappings
Plug 'majutsushi/tagbar' " object viewer
Plug 'lvht/tagbar-markdown' " markdown extension to tagbar
Plug 'tpope/vim-vinegar' " netrw enhancements
Plug 'scrooloose/nerdtree' " file drawer
Plug 'Xuyuanp/nerdtree-git-plugin' " git enhancement
Plug 'luochen1990/rainbow' " Rainbow parentheses, brackets, etc
Plug 'kshenoy/vim-signature' " Show marks in gutter
Plug 'tpope/vim-rsi' " readline key bindings
Plug 'tpope/vim-unimpaired' " common complementary mappings
Plug 'tpope/vim-surround' " gain understanding of 'surroundings'
Plug 'tpope/vim-dispatch' " asynchronous build and test dispatcher
Plug 'tpope/vim-dadbod' " Modern database interface for Vim
Plug 'vim-scripts/matchit.zip' " Match parens, braces, etc
Plug 'lilydjwg/colorizer' " highlight #hex, rgb() mentions
Plug 'scrooloose/nerdcommenter' " handy commenting mappings
Plug 'morhetz/gruvbox' " color scheme
Plug 'ncm2/ncm2'  " Slim, Fast and Hackable Completion Framework for Neovim
Plug 'ncm2/ncm2-bufword'  " provides words from current buffer for completion.
Plug 'ncm2/ncm2-tmux'  " provides completions from other tmux panes
Plug 'ncm2/ncm2-path'  " path completion support
Plug 'ncm2/ncm2-neoinclude'  " include completion framework
Plug 'ncm2/ncm2-jedi'  " Python support for nvim completion manager 2
Plug 'ncm2/ncm2-pyclang'  " Cached, fast C/C++ completion for ncm2 
Plug 'fgrsnau/ncm2-aspell'  " ncm2 source that completes words from aspell dictionary 
Plug 'roxma/nvim-yarp'  " Yet Another Remote Plugin Framework for Neovim
Plug 'Shougo/neoinclude.vim'  " dependency for ncm2-neoinclude
Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh \| UpdateRemotePlugins' } " Unified debugger interface for GDB/LLDB/PDB
Plug 'arakashic/chromatica.nvim', {'for': ['c', 'cpp']} " clang highlighting
Plug 'tweekmonster/braceless.vim' " Python text object, folding, etc., support
Plug 'itchyny/lightline.vim' " Sleek (n)vim UI
Plug 'taohexxx/lightline-buffer' " Extend lightline to buffers
Plug 'mgee/lightline-bufferline'
Plug 'ryanoasis/vim-devicons'  " Add fancy icon support for filetypes
Plug 'tmhedberg/SimpylFold' " Python-specific folding support
Plug 'ludovicchabant/vim-gutentags' " Automatic tag management
Plug 'alcesleo/vim-uppercase-sql'
Plug 'lepture/vim-jinja'
Plug 'bfredl/nvim-ipy'  " IPython interface
Plug 'vimwiki/vimwiki'  " Personal Wiki
Plug 'edkolev/promptline.vim' " Shell prompt generation
Plug 'edkolev/tmuxline.vim' " Tmux UI generation
Plug 'ArtBIT/vim-modularvimrc' " Cascade loading of .vimrc files
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " Fuzzy finder
Plug 'junegunn/fzf.vim'
call plug#end()
