" Basic text editing options
set modeline modelines=5 wildmenu noshowmode laststatus=2 number nowrap
set ttimeoutlen=20
set smarttab expandtab tabstop=8 softtabstop=4
set autoindent shiftwidth=4 backspace=indent,eol,start
set incsearch hlsearch ignorecase
set noswapfile 
set mouse=a
set exrc
set secure
