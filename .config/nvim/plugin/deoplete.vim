let g:deoplete#sources#clang#libclang_path = '/usr/lib/llvm-4.0/lib/libclang.so'
let g:deoplete#sources#clang#clang_header = '/usr/include/clang/4.0/include'

let g:deoplete#sources#jedi#show_docstring = 1
let g:deoplete#sources#jedi#extra_path = ['/usr/lib/freecad/lib']

