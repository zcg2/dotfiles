" denite.nvim
" Change file_rec command.
" For ripgrep
" Note: It is slower than ag
" call denite#custom#var('file/rec', 'command',
" \ ['rg', '--files', '--glob', '!.git', ''])

" Change matchers.
" call denite#custom#source(
" \ 'file_mru', 'matchers', ['matcher/fuzzy', 'matcher/project_files'])
" call denite#custom#source(
" \ 'file/rec', 'matchers', ['matcher/cpsm'])

" Change sorters.
" call denite#custom#source(
" \ 'file/rec', 'sorters', ['sorter/sublime'])

" Add custom menus
let s:menus = {}

let s:menus.my_commands = {
        \ 'description': 'Example commands'
        \ }
let s:menus.my_commands.command_candidates = [
        \ ['Split the window', 'vnew'],
        \ ['Open zsh menu', 'Denite menu:zsh'],
        \ ]

" call denite#custom#var('menu', 'menus', s:menus)

" Ripgrep command on grep source
" call denite#custom#var('grep', 'command', ['rg'])
" call denite#custom#var('grep', 'default_opts',
                " \ ['-i', '--vimgrep', '--no-heading', '--type-not=ts'])
" call denite#custom#var('grep', 'recursive_opts', [])
" call denite#custom#var('grep', 'pattern_opt', ['--regexp'])
" call denite#custom#var('grep', 'separator', ['--'])
" call denite#custom#var('grep', 'final_opts', [])

" Define alias
" call denite#custom#alias('source', 'file/rec/git', 'file/rec')
" call denite#custom#var('file/rec/git', 'command',
      " \ ['git', 'ls-files', '-co', '--exclude-standard'])

" Change default prompt
" call denite#custom#option('default', 'prompt', '>')

" Change ignore_globs
" call denite#custom#filter('matcher/ignore_globs', 'ignore_globs',
      " \ [ '.git/', '.ropeproject/', '__pycache__/',
      " \   'venv/', 'images/', '*.min.*', 'img/', 'fonts/'])
