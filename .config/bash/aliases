#!/bin/bash
# Bash Enhancements
alias ls='ls -hF --group-directories-first --color=always'
[[ -a $HOME/.cargo/bin/exa ]] || [[ -a /usr/bin/exa ]] && alias ls=exa
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -I'

# Coreutils
alias info='info --vi-keys'
alias grep='grep --color'

# Git; handle rest in ~/.gitconfig
# Git is important enough to merit promotion to a single-letter alias in my opinion.
alias g='git'

# Misc
alias udcar='sudo apt update && sudo apt upgrade && sudo apt full-upgrade && sudo apt autoclean && sudo apt autoremove'
alias i='ipython3'
alias mkenv='python3 -m venv .venv && echo source .venv/bin/activate > .envrc && direnv allow'
alias rmenv='rm -rf .venv .envrc'
alias xo='xdg-open'
alias ..='cd ..'
alias ...='cd ..; cd ..'

# systemd
alias sc='sudo systemctl'
alias scu='systemctl --user'

# virsh
alias vco='sudo virsh console'
alias vls='sudo virsh list'
alias vla='sudo virsh list --all'
alias vss='sudo virsh start'
alias vsh='sudo virsh shutdown'
alias vsr='sudo virsh reboot'

# Ansible
alias ap='ansible-playbook'
alias ans-up='for f in ~/.ansible/plays/domains/*; do cd $f && ansible-playbook setup.yml; done; cd'

# LXC/LXD
alias lxca='sudo lxc-attach'
alias lxcc='sudo lxc-create -t download -n'
alias lxci='sudo lxc-info'
alias lxcl='sudo lxc-ls --fancy'
alias lxcr='sudo lxc-destroy'
alias lxcs='sudo lxc-start'
alias lxct='sudo lxc-stop'
